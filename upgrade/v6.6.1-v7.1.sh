#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
# 因为要重启，所以这个也不用结束了
# ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9


############支持包##############

# translate
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.translate-*.jar
wget http://down.zvo.cn/wangmarket/plugin/translate/wangmarket.plugin.translate-1.4.jar

# wangmarket
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
wget http://down.zvo.cn/wangmarket/version/v7.1/wangmarket-7.1.0.jar

# wm
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
wget http://down.zvo.cn/wangmarket/version/v7.1/wm-3.20.jar


# xnx3-util-1.18.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-util-*.jar
wget http://down.zvo.cn/wangmarket/version/v7.1/xnx3-util-1.22.jar

# ainewspublish
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.ainewspublish-*.jar
wget http://down.zvo.cn/wangmarket/plugin/ainewspublish/wangmarket.plugin.ainewspublish-1.0.jar

# autoPublish-1.6.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/autoPublish-*.jar
wget http://down.zvo.cn/wangmarket/plugin/ainewspublish/autoPublish-1.6.jar


# 重启tomcat
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/shell/restart_tomcat.sh -O /mnt/tomcat8/bin/restart_tomcat.sh && chmod -R 777 /mnt/tomcat8/bin/restart_tomcat.sh && nohup /mnt/tomcat8/bin/restart_tomcat.sh &
