#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9

############## 基础支持包等更新 ###############
# 更新wangmarket
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v5.6/wangmarket-5.6.16.jar
# 更新基础支持包 wm.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/wm-2.23.jar
# 更新基础支持包 xnx3-util.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-util-*.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/xnx3-util-1.10.jar
# 更新基础支持包 xnx3.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-2.*.jar
wget http://down.zvo.cn/wangmarket/version/v5.6/xnx3-2.4.jar


############## 插件更新 ###############
# 安装数据库自动备份插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.databaseAutoBackups-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/databaseAutoBackups/wangmarket.plugin.databaseAutoBackups-1.0.jar

# 安装后台开放API
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.adminapi-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/adminapi/wangmarket.plugin.adminapi-1.0.2.jar

# 关键词替换插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/keyword/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/keyword/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.keyword-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/keyword/wangmarket.plugin.keyword-1.3.jar

# 网站分离插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/htmlSeparate/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/htmlSeparate/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/htmlSeparate/wangmarket.plugin.htmlSeparate-1.9.jar
# v5.7版本的网站分离-ftp推送功能，需要这个包实现ftp传输
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/commons-net-*.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/commons-net-3.3.jar

# 万能表单插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/form/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/form/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.form-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/form/wangmarket.plugin.form-1.5.jar

# sitemap
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/sitemap/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/sitemap/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.sitemap-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/sitemap/wangmarket.plugin.sitemap-1.4.jar

# 文件管理
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.fileManager-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/fileManager/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/fileManager/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.fileManager-*.jar
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	wget http://down.zvo.cn/wangmarket/plugin/fileManager/wangmarket.plugin.fileManager-1.2.2.jar
fi

# OEM
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.oem-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/oem/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/oem/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.oem-*.jar
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	wget http://down.zvo.cn/wangmarket/plugin/oem/wangmarket.plugin.oem-1.2.jar
fi


# 安装网站开放API
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteapi-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/siteapi/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/siteapi/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteapi-*.jar
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	wget http://down.zvo.cn/wangmarket/plugin/siteapi/wangmarket.plugin.siteapi-2.0.1.jar
fi

# 子账号
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteSubAccount-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/siteSubAccount/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/siteSubAccount/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteSubAccount-*.jar
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	wget http://down.zvo.cn/wangmarket/plugin/siteSubAccount/wangmarket.plugin.siteSubAccount-1.5.jar
fi

# 文章同步插件
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.newsSync-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/newsSync/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/newsSync/
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.newsSync-*.jar
	cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
	wget http://down.zvo.cn/wangmarket/plugin/newsSync/wangmarket.plugin.newsSync-1.4.jar
fi

# controllerRequestLog插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/controllerRequestLog/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/controllerRequestLog/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.controllerRequestLog-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/controllerRequestLog/wangmarket.plugin.controllerRequestLog-1.2.jar

# 在线客服插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/kefu/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/kefu/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.kefu-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/kefu/wangmarket.plugin.kefu-1.0.jar

# 私有模板库插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/templateCenter/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/templateCenter/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.templateCenter-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/templateCenter/wangmarket.plugin.templateCenter-1.4.jar

# 网站转移插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/siteTransfer/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/siteTransfer/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteTransfer-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/siteTransfer/wangmarket.plugin.siteTransfer-1.2.jar

# robots插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/robots/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/robots/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.robots-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/robots/wangmarket.plugin.robots-1.0.jar

# requestLog插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/requestLog/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/requestLog/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.requestLog-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/requestLog/wangmarket.plugin.requestLog-1.0.jar

# 手机号开通网站插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/com/xnx3/wangmarket/plugin/phoneCreateSite/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/view/plugin/phoneCreateSite/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.phoneCreateSite-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/plugin/phoneCreateSite/wangmarket.plugin.phoneCreateSite-1.4.jar

# 版本升级 插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.upgrade-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
#wget http://down.zvo.cn/wangmarket/plugin/upgrade/wangmarket.plugin.upgrade-1.4.jar?t=20221104_1514


############## 数据库方面更新 ###############
wget http://down.zvo.cn/properties/properties-1.0.1.jar -O ~/properties.jar
# 数据库IP
database_ip=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.ip`
# 数据库名
database_name=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.name`
# 数据库登录用户名
database_username=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.username`
# 数据库登录密码
database_password=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.password`
####### 数据库执行相关命令
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "ALTER TABLE site_column MODIFY COLUMN code_name char(60);"
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "UPDATE system SET value='//res.zvo.cn/' WHERE name='STATIC_RESOURCE_PATH' AND (value='/' OR value = '//res.weiunity.com/') "


# 启动tomcat
cd /mnt/tomcat8/bin/
./startup.sh
