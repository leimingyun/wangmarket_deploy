#!/bin/bash 

# 结束tomcat
cd /mnt/tomcat8/bin/
./shutdown.sh
# 结束 root 用户 执行的所有 java 进程的命令（除了本身grep的这个之外）,免得没能成功结束tomcat
#ps -ef | grep java | grep root | grep -v grep |awk '{print $2}' | xargs --no-run-if-empty kill -9

############## 删除用不到的包 ###############
# activation-1.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/activation-*.jar
#
# aether-api-1.7.jar
# aether-impl-1.7.jar
# aether-spi-1.7.jar
# aether-util-1.7.jar
#
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/aether-*.jar
# aggs-matrix-stats-client-7.10.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/aggs-matrix-stats-client-*.jar
# alipay-sdk-java-4.16.54.ALL.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/alipay-sdk-java-*.jar
#
# aliyun-java-sdk-core-3.0.7.jar
# aliyun-java-sdk-dysmsapi-1.0.0.jar
# aliyun-java-sdk-green-1.4.0.jar
# aliyun-java-sdk-sts-2.1.6.jar
# aliyun-log-0.6.66.jar
# aliyun-openservices-OTS-2.0.4.jar
# aliyun-sdk-oss-2.8.2.jar
#
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/aliyun-*.jar
# amqp-client-5.13.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/amqp-client-*.jar
# animal-sniffer-annotations-1.17.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/animal-sniffer-annotations-*.jar
# antlr4-runtime-4.7.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/antlr4-runtime-*.jar
# asm-7.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/asm-*.jar
# backport-util-concurrent-3.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/backport-util-concurrent-*.jar
# bcprov-jdk15on-1.62.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/bcprov-jdk15on-*.jar
# checker-qual-2.5.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/checker-qual-*.jar
# checkstyle-8.19.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/checkstyle-*.jar
# classworlds-1.1-alpha-2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/classworlds-*.jar
# commons-chain-1.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/commons-chain-*.jar
# commons-compress-1.19.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/commons-compress-*.jar
# commons-lang3-3.12.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/commons-lang3-*.jar
# commons-validator-1.3.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/commons-validator-*.jar
# compiler-0.9.6.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/compiler-*.jar
# dom4j-1.6.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/dom4j-*.jar
# doxia-core-1.2.jar
# doxia-decoration-model-1.4.jar
# doxia-integration-tools-1.6.jar
# doxia-logging-api-1.4.jar
# doxia-module-fml-1.4.jar
# doxia-module-xhtml-1.4.jar
# doxia-sink-api-1.4.jar
# doxia-site-renderer-1.4.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/doxia-*.jar
# elasticsearch-1.1.4.jar
# elasticsearch-7.10.1.jar
# elasticsearch-cli-7.10.1.jar
# elasticsearch-core-7.10.1.jar
# elasticsearch-geo-7.10.1.jar
# elasticsearch-rest-client-7.15.2.jar
# elasticsearch-rest-high-level-client-7.10.1.jar
# elasticsearch-secure-sm-7.10.1.jar
# elasticsearch-x-content-7.10.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/elasticsearch-*.jar
# error_prone_annotations-2.2.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/error_prone_annotations-*.jar
# failureaccess-1.0.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/failureaccess-*.jar
# fastjson-1.2.71.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fastjson-*.jar
# gson-2.8.9.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/gson-*.jar
# guava-27.1-jre.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/guava-*.jar
# hamcrest-2.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/hamcrest-*.jar
# hamcrest-core-2.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/hamcrest-core-*.jar
# HdrHistogram-2.1.9.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/HdrHistogram-*.jar
# hppc-0.8.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/hppc-*.jar
# httpasyncclient-4.1.4.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/httpasyncclient-*.jar
# httpclient-4.5.13.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/httpclient-*.jar
# httpcore-4.4.14.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/httpcore-*.jar
# httpcore-nio-4.4.14.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/httpcore-nio-*.jar
# icu4j-63.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/icu4j-*.jar
# j2objc-annotations-1.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/j2objc-annotations-*.jar
# jackson-dataformat-cbor-2.13.0.jar
# jackson-dataformat-smile-2.13.0.jar
# jackson-dataformat-yaml-2.13.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jackson-dataformat-*.jar
# javax.activation-api-1.2.0.jar
# javax.annotation-api-1.3.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/javax.*.jar
# jaxb-api-2.3.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jaxb-api-*.jar
# jdom-1.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jdom-*.jar
# jna-5.5.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jna-*.jar
# joda-time-2.10.4.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/joda-time-*.jar
# jopt-simple-5.0.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jopt-simple-*.jar
# jsr305-2.0.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/jsr305-*.jar
# junit-4.13.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/-*.jar
# lang-mustache-client-7.10.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/lang-mustache-client-*.jar
# listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/listenablefuture-*.jar
# lucene-analyzers-common-8.7.0.jar
# lucene-backward-codecs-8.7.0.jar
# lucene-core-8.7.0.jar
# lucene-grouping-8.7.0.jar
# lucene-highlighter-8.7.0.jar
# lucene-join-8.7.0.jar
# lucene-memory-8.7.0.jar
# lucene-misc-8.7.0.jar
# lucene-queries-8.7.0.jar
# lucene-queryparser-8.7.0.jar
# lucene-sandbox-8.7.0.jar
# lucene-spatial3d-8.7.0.jar
# lucene-spatial-extras-8.7.0.jar
# lucene-suggest-8.7.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/lucene-*.jar
# lz4-1.3.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/lz4-*.jar
# mail-1.4.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/mail-*.jar
# mapper-extras-client-7.10.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/mapper-extras-client-*.jar
# maven-aether-provider-3.0.jar
# maven-archiver-3.5.0.jar
# maven-artifact-3.0.jar
# maven-artifact-manager-2.2.1.jar
# maven-artifact-transfer-0.10.1.jar
# maven-checkstyle-plugin-3.1.0.jar
# maven-common-artifact-filters-3.0.0.jar
# maven-core-3.0.jar
# maven-invoker-3.0.0.jar
# maven-javadoc-plugin-3.2.0.jar
# maven-model-3.0.jar
# maven-model-builder-3.0.jar
# maven-plugin-annotations-3.5.jar
# maven-plugin-api-3.0.jar
# maven-plugin-descriptor-2.2.1.jar
# maven-plugin-registry-2.2.1.jar
# maven-profile-2.2.1.jar
# maven-project-2.2.1.jar
# maven-reporting-api-3.0.jar
# maven-reporting-impl-2.3.jar
# maven-repository-metadata-3.0.jar
# maven-settings-3.0.jar
# maven-settings-builder-3.0.jar
# maven-shared-utils-0.6.jar
# maven-source-plugin-3.2.1.jar
# maven-surefire-common-2.22.0.jar
# maven-surefire-plugin-2.22.0.jar
# maven-toolchain-2.2.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/maven-*.jar
# oro-2.0.8.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/oro-*.jar
# parent-join-client-7.10.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/parent-join-client-*.jar
# picocli-3.9.5.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/picocli-*.jar
# plexus-archiver-4.2.1.jar
# plexus-cipher-1.4.jar
# plexus-classworlds-2.2.3.jar
# plexus-component-annotations-1.7.1.jar
# plexus-container-default-1.0-alpha-9.jar
# plexus-i18n-1.0-beta-7.jar
# plexus-interactivity-api-1.0-alpha-6.jar
# plexus-interpolation-1.24.jar
# plexus-io-3.2.0.jar
# plexus-java-1.0.5.jar
# plexus-resources-1.1.0.jar
# plexus-sec-dispatcher-1.3.jar
# plexus-utils-3.0.24.jar
# plexus-velocity-1.1.8.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/plexus-*.jar
# protobuf-java-2.5.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/protobuf-java-*.jar
# qdox-2.0-M10.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/qdox-*.jar
# rabbitmq-1.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/rabbitmq-*.jar
# rank-eval-client-7.10.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/rank-eval-client-*.jar
# Saxon-HE-9.9.1-2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/Saxon-HE-*.jar
# sisu-guice-2.1.7-noaop.jar
#sisu-inject-bean-1.4.2.jar
#sisu-inject-plexus-1.4.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/sisu-*.jar
# snappy-0.4.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/snappy-*.jar
# spring-amqp-2.4.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/spring-amqp-*.jar
# spring-boot-starter-amqp-2.6.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/spring-boot-starter-amqp-*.jar
# spring-messaging-5.3.13.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/spring-messaging-*.jar
# spring-rabbit-2.4.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/spring-rabbit-*.jar
# spring-retry-1.3.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/spring-retry-*.jar
# sslext-1.2-0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/sslext-*.jar
# struts-core-1.3.8.jar
# struts-taglib-1.3.8.jar
# struts-tiles-1.3.8.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/struts-*.jar
# surefire-api-2.22.0.jar
# surefire-booter-2.22.0.jar
# surefire-logger-api-2.22.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/surefire-*.jar
# t-digest-3.2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/t-digest-*.jar
# velocity-1.5.jar
# velocity-tools-2.0.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/velocity-*.jar
# wagon-provider-api-2.4.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wagon-provider-api-*.jar
# wangmarket.plugin.form-1.5.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.form-*.jar
# wangmarket.plugin.htmlSeparate-1.9.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar
# wangmarket.plugin.huaWeiYunServiceCreate-1.6.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.huaWeiYunServiceCreate-*.jar
# wangmarket-5.7.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-5.7.jar
# wm-2.25.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-2.25.jar
# xercesImpl-2.9.1.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xercesImpl-*.jar
# xml-apis-1.0.b2.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xml-apis-*.jar
# xnx3-2.7.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-2.7.jar
# xnx3-util-1.10.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-util-*.jar
# xnx3-weixin-1.7.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-weixin-1.7.jar
# xz-1.8.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xz-1.8.jar


############## 基础支持包等更新 ###############
# 更新wangmarket
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket-*.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
wget http://down.zvo.cn/wangmarket/version/v6.0/wangmarket-6.0.jar
# 更新基础支持包 wm.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wm-*.jar
wget http://down.zvo.cn/wangmarket/version/v6.0/wm-2.34.jar
# 更新基础支持包 xnx3-util.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-util-*.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/xnx3-util-1.14.jar
# 更新基础支持包 xnx3.jar
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/xnx3-2.*.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/xnx3-2.8.jar

# wm用到的fastjson相关
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fastjson-2.0.19.graal.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/fastjson-2.0.19.graal.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fastjson2-2.0.19.graal.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/fastjson2-2.0.19.graal.jar
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fastjson2-extension-2.0.19.graal.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/fastjson2-extension-2.0.19.graal.jar

############## 加入v6.0的相关支持包 ###############
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/

rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/fileupload-*.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/fileupload-core-1.1.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/fileupload-framework-springboot-1.1.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/fileupload-framework-ueditor-1.0.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/fileupload-storage-huaweicloudOBS-1.0.jar

rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/log-*.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/log-core-1.1.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/log-framework-springboot-1.0.jar

rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/page-1.*0.jar
wget http://down.zvo.cn/wangmarket/version/v6.0/page-1.0.jar

rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/http-1.*0.jar
wget http://down.zvo.cn/wangmarket/version/v5.7/http-1.0.jar



############## 插件更新 ###############
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/

# form
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.form-*.jar
wget http://down.zvo.cn/wangmarket/plugin/form/wangmarket.plugin.form-1.8.jar

# htmlSeparate
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.htmlSeparate-*.jar
wget http://down.zvo.cn/wangmarket/plugin/htmlSeparate/wangmarket.plugin.htmlSeparate-1.11.jar

# huaWeiYunServiceCreate
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.huaWeiYunServiceCreate-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.huaWeiYunServiceCreate-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/huaWeiYunServiceCreate/wangmarket.plugin.huaWeiYunServiceCreate-1.8.jar
fi

# siteSubAccount
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteSubAccount-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteSubAccount-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/siteSubAccount/wangmarket.plugin.siteSubAccount-1.6.jar
fi

# fileManager
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.fileManager-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.fileManager-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/fileManager/wangmarket.plugin.fileManager-1.3.jar
fi

# siteTransfer
if ls /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteTransfer-*.jar 2>&1;then
	rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.siteTransfer-*.jar
	wget http://down.zvo.cn/wangmarket/plugin/siteTransfer/wangmarket.plugin.siteTransfer-1.4.jar
fi

# requestLog
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.requestLog-*.jar

# 加入国际化插件
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.translate-*.jar
wget http://down.zvo.cn/wangmarket/plugin/translate/wangmarket.plugin.translate-1.0.jar

# upgrade
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/lib/wangmarket.plugin.upgrade-*.jar
wget http://down.zvo.cn/wangmarket/plugin/upgrade/wangmarket.plugin.upgrade-1.1.jar

# 客服插件的客服模板安装
mkdir /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/ -p
rm -rf /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/*
cd /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/static/plugin/kefu/template/
wget http://down.zvo.cn/wangmarket/plugin/kefu/template_v1.0.zip -O template.zip
yum -y install unzip
unzip template.zip
rm -rf template.zip


############## 数据库方面更新 ###############
wget http://down.zvo.cn/properties/properties-1.0.1.jar -O ~/properties.jar
# 数据库IP
database_ip=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.ip`
# 数据库名
database_name=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get database.name`
# 数据库登录用户名
database_username=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.username`
# 数据库登录密码
database_password=`java -cp ~/properties.jar Properties -path=/mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties -get spring.datasource.password`
####### 数据库执行相关命令
# 栏目的code字段加长
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "ALTER TABLE site_column MODIFY COLUMN code_name char(60);"
# 更新系统云端资源，有 res.weiunity.com 变为 res.zvo.cn
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "UPDATE system SET value='//res.zvo.cn/' WHERE name='STATIC_RESOURCE_PATH' AND (value='/' OR value = '//res.weiunity.com/') "
# 去掉日志管理-访问统计菜单
mysql -h $database_ip -u$database_username -p$database_password $database_name -e "UPDATE permission SET menu = '0' WHERE percode = 'adminLogCartogram'"


# 启动tomcat
reboot
